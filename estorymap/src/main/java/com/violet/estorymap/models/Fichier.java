package com.violet.estorymap.models;

import jakarta.persistence.*;

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
public class Fichier {
	
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="Seq_Fichier")
    @SequenceGenerator(name="Seq_Fichier", sequenceName="Seq_Fichier", allocationSize=1)
    @Column(name="id_Fichier") 
    private Long idFichier;
    public Long getIdFichier(){return this.idFichier;}
    public void setIdFichier( Long i) { this .idFichier = i;}

    
    
    @Column 
    private String typeFichier;
    public void setTypeFichier(String f) {this.typeFichier =f;}
    public String getTypeFichier() {return this.typeFichier;} 
    
	@ManyToOne
	@JoinColumn (name="id_projet")
	private Projet projet;    
	public Projet getProjet() {
		return projet;
	}
	public void setProjet(Projet projet) {
		this.projet = projet;
	
    }
}
