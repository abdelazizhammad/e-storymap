package com.violet.estorymap.models;

import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.*;



@Entity
@PrimaryKeyJoinColumn(name="id_Utilisateur_Invite")

public class Invite extends Utilisateur{
	
	
	
    @OneToMany(mappedBy="invite")
    private Set <Projet> listeProjetsInvite = new HashSet <Projet>() ;
    public void addListeProjetsInvite (Projet p) { listeProjetsInvite.add (p) ;}
    public Set <Projet> getListeProjetsInvite () { return listeProjetsInvite;}
    public void removeListeProjetsInvite ( Projet p) { listeProjetsInvite.remove (p);}
    
	@ManyToMany()
	@JoinTable(name="partage avec",joinColumns=@JoinColumn(name="id_Utilisateur_Invite"),
				inverseJoinColumns=@JoinColumn(name="id_Utilisateur_Proprietaire"))
	Set<Proprietaire> lesProprietaires = new HashSet<Proprietaire>();
	public Set<Proprietaire> getLesProprietaires() {return lesProprietaires;}
    public void addProprietaire(Proprietaire p) {lesProprietaires.add(p);}
    
    
}
    