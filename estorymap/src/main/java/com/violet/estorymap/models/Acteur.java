package com.violet.estorymap.models;


import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.*;

@Entity
public class Acteur {
	
    @Id
    //@GeneratedValue(strategy=GenerationType.AUTO)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="Seq_Acteur")
    @SequenceGenerator(name="Seq_Acteur", sequenceName="Seq_Acteur", allocationSize=1)
    @Column(name="id_Acteur") 
    private Long idActeur;
    public Long getIdActeur(){return this.idActeur;}
    public void setIdActeur( Long i) { this .idActeur = i;}

    
    @Column 
    private String nomActeur;
    public void setNomActeur(String n) {this.nomActeur =n;}
    public String getNomActeur() {return this.nomActeur;} 
    
    @ManyToMany(mappedBy ="lesActeursMcta")
    private Set<Mcta> listeDesMcta = new HashSet<Mcta>();
	public Set<Mcta> getListeDesMcta() {return listeDesMcta;}
    public void addMcta(Mcta m) {listeDesMcta.add(m);}
	
    
    @ManyToMany(mappedBy ="lesActeursBpmn")
    private Set<Bpmn> listeDesBpmn = new HashSet<Bpmn>();
	public Set<Bpmn> getListeDesBpmn() {return listeDesBpmn;}
    public void addBpmn(Bpmn b) {listeDesBpmn.add(b);}
    
    
    @ManyToMany(mappedBy ="lesActeursMfc")
    private Set<Mfc> listeDesMfc = new HashSet<Mfc>();
	public Set<Mfc> getListeDesMfc() {return listeDesMfc;}
    public void addMFc(Mfc m) {listeDesMfc.add(m);}
	

}
