package com.violet.estorymap.models;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.*;

@Entity
public class Flux {
	 
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="Seq_Flux")
    @SequenceGenerator(name="Seq_Flux", sequenceName="Seq_Flux", allocationSize=1)
	@Column(name="id_Flux") 
	private Long idFlux;
	public Long getIdFlux(){return this.idFlux;}
	public void setIdFlux( Long i) { this .idFlux = i;}

	    
	@Column 
	private String message;
	public void setMessage(String m) {this.message =m;}
	public String getMessage() {return this.message;} 
	
    @ManyToMany(mappedBy ="lesFluxMcta")
    private List<Mcta> listeDesMcta = new ArrayList<Mcta>();
	public List<Mcta> getListeDesMcta() {return listeDesMcta;}
    public void addMcta(Mcta m) {listeDesMcta.add(m);}
	
    
    @ManyToMany(mappedBy ="lesFluxBpmn")
    private List<Bpmn> listeDesBpmn = new ArrayList<Bpmn>();
	public List<Bpmn> getListeDesBpmn() {return listeDesBpmn;}
    public void addBpmn(Bpmn b) {listeDesBpmn.add(b);}
    
    
    @ManyToMany(mappedBy ="lesFluxMfc")
    private List<Mfc> listeDesMfc = new ArrayList<Mfc>();
	public List<Mfc> getListeDesMfc() {return listeDesMfc;}
    public void addMFc(Mfc m) {listeDesMfc.add(m);}


	
		
	}
	   
	 


