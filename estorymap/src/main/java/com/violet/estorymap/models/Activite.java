package com.violet.estorymap.models;

import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.*;


@Entity
public class Activite {
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="Seq_Activite")
    @SequenceGenerator(name="Seq_Acticite", sequenceName="Seq_Activite", allocationSize=1)
    @Column(name="id_Activite") 
    private Long idActivite;
    public Long getIdActivite(){return this.idActivite;}
    public void setIdActivite( Long i) { this .idActivite = i;}

    
    @Column 
    private String nomActivite;
    public void setNomActivite(String n) {this.nomActivite =n;}
    public String getNomActivite() {return this.nomActivite;} 
    
    @ManyToMany(mappedBy ="lesActivitesMcta")
    private Set<Mcta> listeDesMcta = new HashSet<Mcta>();
	public Set<Mcta> getListeDesMcta() {return listeDesMcta;}
    public void addMcta(Mcta m) {listeDesMcta.add(m);}
    
    @ManyToMany(mappedBy ="lesActivitesBpmn")
    private Set<Bpmn> listeDesBpmn = new HashSet<Bpmn>();
	public Set<Bpmn> getlisteDesBpmn() {return listeDesBpmn;}
    public void addBpmn(Bpmn b) {listeDesBpmn.add(b);}

}
