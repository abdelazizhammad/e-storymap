package com.violet.estorymap.models;

import jakarta.persistence.*;

@Entity
public class Utilisateur {
	
	@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="Seq_Utilisateur")
    @SequenceGenerator(name="Seq_Utilisateur", sequenceName="Seq_Utilisateur", allocationSize=1)
    @Column(name="id_Utilisateur") 
    private Long idUtilisateur;
    public Long getIdUtilisateur(){return this.idUtilisateur;}
	public void setIdUtilisateurt(Long id) { this.idUtilisateur = id;}	

    @Column 
    private String nomUtilisateur;
    public void setNomUtilisateur(String n) {this.nomUtilisateur =n;}
    public String getNomUtilisateur() {return this.nomUtilisateur;} 
    
    @Column 
    private String prenomUtilisateur;
    public void setPrenomUtilisateur(String p) {this.prenomUtilisateur =p;}
    public String getPrenomUtilisateur() {return this.prenomUtilisateur;} 
    
    @Column 
    private String adresseMail;
    public void setAdresseMail(String e) {this.adresseMail =e;}
    public String getAdresseMail() {return this.adresseMail;} 
    
    @Column 
    private String pseudonyme;
    public void setPseudonyme(String p) {this.pseudonyme=p;}
    public String getPseudonyme() {return this.pseudonyme;}

}

