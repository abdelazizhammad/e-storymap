package com.violet.estorymap.models;

import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.*;

@Entity
@PrimaryKeyJoinColumn(name="id_Utilisateur_Proprietaire")

public class Proprietaire extends Utilisateur{
	
    @ManyToMany(mappedBy ="lesProprietaires")
    private Set<Invite> listeDesInvites = new HashSet<Invite>();
	public Set<Invite> getListeDesInvites() {return listeDesInvites;}
    public void addInvite(Invite i) {listeDesInvites.add(i);}
    
    @OneToMany(mappedBy="proprietaire")
    private Set <Projet> projets = new HashSet <Projet>() ;
    public void addProjet (Projet p) { projets.add (p) ;}
    public Set <Projet> getProjets () { return projets;}
    public void removeProjet ( Projet p) { projets.remove (p);}
	

}
