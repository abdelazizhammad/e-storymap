package com.violet.estorymap.models;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.*;

@Entity
@PrimaryKeyJoinColumn(name="id_Fichier")

public class Mfc extends Fichier{
	
	@ManyToMany()
	@JoinTable(name="est compose",joinColumns=@JoinColumn(name="id_Fichier"),
				inverseJoinColumns=@JoinColumn(name="id_flux"))
	List<Flux> lesFluxMfc = new ArrayList<Flux>();
	public List<Flux> getlesFlux() {return lesFluxMfc;}
    public void addFluxMfc(Flux f) {lesFluxMfc.add(f);}
    
	@ManyToMany()
	@JoinTable(name="contient",joinColumns=@JoinColumn(name="id_Fichier"),
				inverseJoinColumns=@JoinColumn(name="id_acteur"))
	List<Acteur> lesActeursMfc = new ArrayList<Acteur>();
	public List<Acteur> getlesActeurs() {return lesActeursMfc;}
    public void addActeurs(Acteur a) {lesActeursMfc.add(a);}
    
    
}

