package com.violet.estorymap.models;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.*;

@Entity
@PrimaryKeyJoinColumn(name="id_Fichier")

public class Mcta extends Fichier{
	
	@ManyToMany()
	@JoinTable(name="est dans",joinColumns=@JoinColumn(name="id_Fichier"),
				inverseJoinColumns=@JoinColumn(name="id_activite"))
	List<Activite> lesActivitesMcta = new ArrayList<Activite>();
	public List<Activite> getlesActiviesMcta() {return lesActivitesMcta;}
    public void addActiviteMcta(Activite a) {lesActivitesMcta.add(a);}
    
	@ManyToMany()
	@JoinTable(name="est constitué",joinColumns=@JoinColumn(name="id_Fichier"),
				inverseJoinColumns=@JoinColumn(name="id_flux"))
	List<Flux> lesFluxMcta = new ArrayList<Flux>();
	public List<Flux> getlesFluxMcta() {return lesFluxMcta;}
    public void addFluxMcta(Flux f) {lesFluxMcta.add(f);}
    
	@ManyToMany()
	@JoinTable(name="fait parti",joinColumns=@JoinColumn(name="id_Fichier"),
				inverseJoinColumns=@JoinColumn(name="id_acteur"))
	List<Acteur> lesActeursMcta = new ArrayList<Acteur>();
	public List<Acteur> getlesActeursMcta() {return lesActeursMcta;}
    public void addActeurMcta(Acteur p) {lesActeursMcta.add(p);}

}

