package com.violet.estorymap.models;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.*;



@Entity
@PrimaryKeyJoinColumn(name="id_Fichier")
public class Bpmn extends Fichier{

	
	@ManyToMany()
	@JoinTable(name="appartient",joinColumns=@JoinColumn(name="id_Fichier"),
				inverseJoinColumns=@JoinColumn(name="id_activite"))
	List<Activite> lesActivitesBpmn = new ArrayList<Activite>();
	public List<Activite> getlesActivitesBpmn() {return lesActivitesBpmn;}
    public void addActiviteBpmn(Activite a) {lesActivitesBpmn.add(a);}
    
	@ManyToMany()
	@JoinTable(name="est décomposé",joinColumns=@JoinColumn(name="id_Fichier"),
				inverseJoinColumns=@JoinColumn(name="id_acteur"))
	List<Acteur> lesActeursBpmn = new ArrayList<Acteur>();
	public List<Acteur> getlesActeursBpmn() {return lesActeursBpmn;}
    public void addActeurBpmn(Acteur p) {lesActeursBpmn.add(p);}

    
    
	@ManyToMany()
	@JoinTable(name="se décompose",joinColumns=@JoinColumn(name="id_Fichier"),
				inverseJoinColumns=@JoinColumn(name="id_flux"))
	List<Flux> lesFluxBpmn = new ArrayList<Flux>();
	public List<Flux> getlesFluxBpmn() {return lesFluxBpmn;}
    public void addFluxBpmn(Flux f) {lesFluxBpmn.add(f);}
    
    
}
