package com.violet.estorymap.models.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.violet.estorymap.models.Fichier;

@Repository
public interface FichierRepository extends JpaRepository<Fichier,Long>{

	Fichier findByTypeFichier(String typeFichier);

}
