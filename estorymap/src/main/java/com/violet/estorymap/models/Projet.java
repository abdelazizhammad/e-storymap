package com.violet.estorymap.models;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.*;


@Entity
public class Projet implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="Seq_Projet")
	@SequenceGenerator(name="Seq_Projet", sequenceName="Seq_Projet", allocationSize=1)
	private Long idProjet;
	public Long getIdProjet() { return idProjet;}
	public void setIdProjet(Long id) { this.idProjet = id;}	


	
    @Column 
    private String nomProjet;
    public void setNomProjet(String n) {this.nomProjet =n;}
    public String getNomProjet() {return this.nomProjet;} 
    
    @Column 
    private String lienProjet;
    public void setLienProjet(String l) {this.lienProjet =l;}
    public String getLienProjet() {return this.lienProjet;} 
    
    @OneToMany(mappedBy="projet")
    private Set <Fichier> fichiers = new HashSet <Fichier>() ;
    public void addFichier (Fichier f) { fichiers.add (f) ;}
    public Set <Fichier> getFichier () { return fichiers;}
    public void removeFichier ( Fichier f) { fichiers.remove (f);}
 
	@ManyToOne
	@JoinColumn (name="id_utilisateur_invite")
	private Invite invite;    
	public Invite getInvite() {
		return invite;
	}
	public void setInvite(Invite invite) {
		this.invite = invite;
	}
	
	
	@ManyToOne
	@JoinColumn (name="id_utilisateur_proprietaire")
	private Proprietaire proprietaire;    
	public Proprietaire getProprietaire() {
		return proprietaire;
	}
	public void setProprietaire(Proprietaire proprietaire) {
		this.proprietaire = proprietaire;
	}
	
}
