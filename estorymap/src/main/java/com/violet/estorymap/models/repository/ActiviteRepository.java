package com.violet.estorymap.models.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.violet.estorymap.models.Activite;

@Repository
public interface ActiviteRepository  extends JpaRepository<Activite,Long>{

}
