package com.violet.estorymap;

import java.io.File;

import org.camunda.bpm.model.bpmn.impl.BpmnParser;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import com.violet.estorymap.metier.parser.ParserBpmn;


@SpringBootApplication
public class EstorymapApplication {

	public static void main(String[] args) {
		
		SpringApplication.run(EstorymapApplication.class, args);
	// 	File file = new File ("../diagram(7)");
	// 	ParserBpmn parser=new ParserBpmn(file);
	// 	parser.getFlux();
	// 	parser.getAuthors();
	// 	parser.getAllTasks();
	}

}
