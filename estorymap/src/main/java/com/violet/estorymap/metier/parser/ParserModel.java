package com.violet.estorymap.metier.parser;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/*
 * parser pour les MFC et les MCTA
 */


public class ParserModel {
    List<String> listeAllElements = new ArrayList<String>();

    List<String> Authors=new ArrayList<String>();
    List<String> Activities=new ArrayList<String>();
    List<String> Flux=new ArrayList<String>();

    public ParserModel(){
    
    }

    /*
     * répartit les vertex qui constituent les acteurs, activités ou flux d'un modèle et les incorpore dans la bonne liste de fichiers
     */
    public void  getAllElements(File file){

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file.getAbsolutePath());            
            NodeList vertexNodes = doc.getElementsByTagName("mxCell");


            for (int i = 0; i < vertexNodes.getLength(); i++) {
                 Element vertex = (Element) vertexNodes.item(i);
                String vertexType = vertex.getAttribute("vertex");
                if (vertexType.equals("1")) {
                 String name = vertex.getAttribute("value");
                    Pattern pattern = Pattern.compile(">([^<]*)<");
                    Matcher matcher = pattern.matcher(name);
                    if (matcher.find()) {
                        String contenu = matcher.group(1);
                        listeAllElements.add(convertString(contenu));

                        tri(convertString(contenu));


                       // System.out.println(convertString(contenu));
                    }
                    else{
                        tri(convertString(name));
                        ;
                        //System.out.println(convertString(name));
                    }

                }
    
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<String> getListAllElements(){
        return this.listeAllElements;
    }

    public String convertString(String n){
        //Supprimer les accents et le :s
        String chaineSansAccents = java.text.Normalizer.normalize(n, java.text.Normalizer.Form.NFD)
        .replaceAll("\\p{InCombiningDiacriticalMarks}+", "").replace(":", "");

        // Convertir en minuscules et supprimer les espaces
        String chaineEnMinusculesSansEspaces = chaineSansAccents.toLowerCase().replaceAll("\\s+", "");

        // Extraire la chaîne 

        String resultat = chaineEnMinusculesSansEspaces.substring(0, chaineEnMinusculesSansEspaces.length());
        
         return resultat;

    }


    public void tri(String s){
        String flux = "^f[0-9].*";
        String authors = "^p[0-9].*";
        String activite = "^a[0-9].*";


        if (s.matches(flux)) {
            this.Flux.add(s);

        }else if(s.matches(authors)){
            this.Authors.add(s);

        }else if (s.matches(activite)){
            this.Activities.add(s);

        }
    }


    public  List<String>getFlux(){
       return   this.Flux;
    }
    
    public void setFlux( List<String> flux) {
    	this.Flux= flux;
    }

    public List<String> getAuthors(){
        return   this.Authors;
    }
    public void setAuthors( List<String> authors) {
    	this.Authors= authors;
    }

    public List<String> getActivity(){
        return this.Activities;

        }
    
    public void setActivity( List<String> activity) {
    	this.Activities= activity;
    }

    }




