package com.violet.estorymap.metier;

import java.util.ArrayList;
import java.util.List;



public class Dico {
    List<String>Flux;
    List<String>Authors;
    List<String>Activities;
    List<String> commonElements;
    List<String> uniqueElements ;

    public  Dico(){ 
        this.Flux=new ArrayList<String>();
        this.Authors=new ArrayList<String>();
        this.Activities=new ArrayList<String>();
    }
    /*
     * verifie les flux  activite ou acteur duplique dans un model  
     */
 

     public void numberDuplication(List<String> myList, List<String>duplicate){
    	 
    	  for (int i = 0; i < myList.size(); i++) {
              for (int j = i + 1; j < myList.size(); j++) {
              	
              	String mystring1 = new String(myList.get(i));
              	String mystring2 = new String((myList.get(j)));
                  if (mystring1.substring(0, 3).equals(mystring2.substring(0, 3))) {
                      duplicate.add("Duplication found: " + myList.get(j));
                      myList.remove(myList.get(j));
                      
                     
                  }
                  
                  
              }
          }  
      }
       
   

    public void Duplication(List<String> myList){
    	
    	
    	 for (int i = 0; i < myList.size(); i++) {
             for (int j = i + 1; j < myList.size(); j++) {

                 if (myList.get(i).equals(myList.get(j))) {
                     System.out.println("Duplication found: " + myList.get(i));
                 }
             }
         }  
    }
      
/*
 * compare deux liste de flux , d'acteur ou d'activite 
 */
    public void compareElements( List<String> list1, List<String> list2){
    this.commonElements = new ArrayList<>(list1);
    commonElements.retainAll(list2);
    
    for (String s:commonElements){
        tri(s);
    }
    List<String> uniqueElements1 = new ArrayList<>(list1);
    List<String> uniqueElements2 = new ArrayList<>(list2);
     uniqueElements1.removeAll(list2);
     uniqueElements2.removeAll(list1);
     uniqueElements1.addAll(uniqueElements2);
     this.uniqueElements=  new ArrayList<>(uniqueElements1);
          for (String s:uniqueElements){
        tri(s);
    }
}
    
    /*
     * la methode remplie les differentes listes (acteur flux et activite en fonction des regex)  
     */
   public void tri(String s){
    String flux = "^f[0-9].*";
    String authors = "^p[0-9].*";
    String activite = "^a[0-9].*";


    if (s.matches(flux)) {
        if (!this.Flux.contains(s)) {
            this.Flux.add(s);
        }
       

    }else if(s.matches(authors)){
        if (!this.Authors.contains(s)) {
            this.Authors.add(s);
        }

    }else if (s.matches(activite)){
        if (!this.Activities.contains(s)) {
            this.Activities.add(s);
        }

    }
}    

public List<String> getTotalFlux(){
    return this.Flux;
}

public List<String> getTotalAuthors(){
    return this.Authors;
}

public List<String> getTotalActivities(){
    return this.Activities;
}

public List<String> getCommonElements(){
    return this.commonElements;
}

public List<String> getUniqueElements(){
    return this.uniqueElements;
}
    
}

        
    

    
