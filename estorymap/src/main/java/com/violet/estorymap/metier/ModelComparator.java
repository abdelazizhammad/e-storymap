package com.violet.estorymap.metier;

import java.util.ArrayList;
import java.util.List;

import com.violet.estorymap.metier.parser.ParserBpmn;
import com.violet.estorymap.metier.parser.ParserModel;

public class ModelComparator {
	private ParserModel mfc;
    private ParserModel bpmn;
    private ParserModel mcta;

    public ModelComparator(ParserModel mfc, ParserModel bpmn, ParserModel mcta) {
        this.mfc = mfc;
        this.bpmn = bpmn;
        this.mcta = mcta;
    }

    public double compareActors() {
        int total = mfc.getAuthors().size();
        int matches = 0;

        for (String actor : mfc.getAuthors()) {
            if (bpmn.getAuthors().contains(actor) || mcta.getAuthors().contains(actor)) {
                matches++;
            }
        }

        return ((double) matches / total) * 100;
    }

    public double compareFluxs() {
        int total = mfc.getFlux().size();
        int matches = 0;

        for (String Flux : mfc.getFlux()) {
            if (bpmn.getFlux().contains(Flux) || mcta.getFlux().contains(Flux)) {
                matches++;
            }
        }

        return ((double) matches / total) * 100;
    }

    public double compareActivities() {
        int total = bpmn.getActivity().size();
        int matches = 0;

        for (String activity : bpmn.getActivity()) {
            if (mcta.getActivity().contains(activity)) {
                matches++;
            }
        }

        return ((double) matches / total) * 100;
    } 

    public  List<Integer>  compareModels() {
    	
    	List<Integer> coherence = new ArrayList<>();
    	
        Integer bpmnCoherence = (int) ((compareActors() + compareFluxs() + compareActivities()) / 3);
        System.out.println("BPMN coherence: " + bpmnCoherence + "%");

        Integer mfcCoherence = (int) ((compareActors() + compareFluxs()) / 2);
        System.out.println("MFC coherence: " + mfcCoherence + "%");

        //double mctaCoherence = compareActivities();
       // System.out.println("MCTA coherence: " + mctaCoherence + "%");
        coherence.add(mfcCoherence);
        coherence.add(bpmnCoherence);
        return  coherence;
        
    }
}


