package com.violet.estorymap.metier.parser;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.IntermediateCatchEvent;
import org.camunda.bpm.model.bpmn.instance.IntermediateThrowEvent;
import org.camunda.bpm.model.bpmn.instance.MessageFlow;
import org.camunda.bpm.model.bpmn.instance.Participant;
import org.camunda.bpm.model.bpmn.instance.Task;
/*
 *  cette class permet de parser un fichier bpmn 
 */

public class ParserBpmn {

    private BpmnModelInstance modelInstance;
    List<String> listeFlux = new ArrayList<String>();
    List<String> listeActivity = new ArrayList<String>();
    List<String> listeAuthors = new ArrayList<String>();
    Collection<IntermediateThrowEvent> flux1 ;
    Collection<IntermediateCatchEvent> flux2 ;  
    Collection<MessageFlow> flux3 ;
    Collection<Participant> authors;
    Collection<Task>tasks;
    

    public ParserBpmn(){}

    public ParserBpmn(File f){
        this.modelInstance = Bpmn.readModelFromFile(f);
        this.flux3=modelInstance.getModelElementsByType(MessageFlow.class);
        this.flux1=modelInstance.getModelElementsByType(IntermediateThrowEvent.class);
        this.flux2=modelInstance.getModelElementsByType(IntermediateCatchEvent.class);
        this.authors=modelInstance.getModelElementsByType(Participant.class);
        this.tasks=modelInstance.getModelElementsByType(Task.class);

    }

    public ParserBpmn(InputStream i){
        this.modelInstance = Bpmn.readModelFromStream(i);
        this.flux3=modelInstance.getModelElementsByType(MessageFlow.class);
        this.flux1=modelInstance.getModelElementsByType(IntermediateThrowEvent.class);
        this.flux2=modelInstance.getModelElementsByType(IntermediateCatchEvent.class);
        this.authors=modelInstance.getModelElementsByType(Participant.class);
        this.tasks=modelInstance.getModelElementsByType(Task.class);
    }

    public List<String> getFlux(){
        getMessageFlows();
        getIntermediateCatchEvent();
        getIntermediateThrowEvent(); 
        return listeFlux; 
    }

    public List<String> getAuthors(){
        for (Participant author: authors){
            if(author.getName() != null){
                listeAuthors.add(convertString(author.getName()));
            }
        }
        return this.listeAuthors;

    }

    public List<String> getTasks(){
        for (Task task: tasks){
            if(task.getName() != null){
                listeActivity.add(convertString(task.getName()));
                
            }
        }
        return this.listeActivity;
    }

    public List<String> getAllTasks(){
        getTasks();
        return this.listeActivity;
    }
    
    public void getMessageFlows(){
        for (MessageFlow m: flux3){
            if(m.getName() != null){
                listeFlux.add(convertString(m.getName()));
               
            }
        }
    }
    /*
     * recupere les flux entrant 
     */
    public void getIntermediateCatchEvent(){
        for(IntermediateCatchEvent m2: flux2){
            if(m2.getName() != null){
                listeFlux.add(convertString(m2.getName()));
               
            }
        }
   }
    /*
     * recupere les flux sortant  
     */
     
    public void getIntermediateThrowEvent(){
        for(IntermediateThrowEvent m3: flux1){
            if(m3.getName() != null){
                listeFlux.add(convertString(m3.getName()));
               
            }
        }
    }
/*
 * convertit la chaine afin de la comparer au autre elements des modeles 
 */
    public String convertString(String n){
        //Supprimer les accents et le :s
        String chaineSansAccents = java.text.Normalizer.normalize(n, java.text.Normalizer.Form.NFD)
        .replaceAll("\\p{InCombiningDiacriticalMarks}+", "").replace(":", "");

        // Convertir en minuscules et supprimer les espaces
        String chaineEnMinusculesSansEspaces = chaineSansAccents.toLowerCase().replaceAll("\\s+", "");

        // Extraire la chaîne 

        String resultat = chaineEnMinusculesSansEspaces.substring(0, chaineEnMinusculesSansEspaces.length());
        
         return resultat;

    }
}
