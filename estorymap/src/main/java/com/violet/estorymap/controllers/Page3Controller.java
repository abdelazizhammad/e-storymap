package com.violet.estorymap.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;



@Controller
public class Page3Controller {
    
    @GetMapping(value="/page3.html")
    public String getPage() {
        return "page3";
    }
    
}
