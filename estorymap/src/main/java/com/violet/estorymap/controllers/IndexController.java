package com.violet.estorymap.controllers;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.violet.estorymap.ldap.ConnectionLdap;
import com.violet.estorymap.metier.Dico;
import com.violet.estorymap.metier.ModelComparator;
import com.violet.estorymap.metier.parser.ParserBpmn;
import com.violet.estorymap.metier.parser.ParserModel;
import com.violet.estorymap.models.Acteur;
import com.violet.estorymap.models.Activite;
import com.violet.estorymap.models.Bpmn;
import com.violet.estorymap.models.Flux;
import com.violet.estorymap.models.Mcta;
import com.violet.estorymap.models.Mfc;
import com.violet.estorymap.models.Utilisateur;
import com.violet.estorymap.models.repository.UtilisateurRepository;

import org.springframework.ui.Model;

import com.violet.estorymap.models.repository.ActeurRepository;
import com.violet.estorymap.models.repository.ActiviteRepository;
import com.violet.estorymap.models.repository.BpmnRepository;
import com.violet.estorymap.models.repository.FluxRepository;
import com.violet.estorymap.models.repository.MctaRepository;
import com.violet.estorymap.models.repository.MfcRepository;
import com.violet.estorymap.models.repository.ProjetRepository;





@Controller
public class IndexController {

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Autowired
    private FluxRepository fluxRepository;
    @Autowired
    private ActiviteRepository activiteRepository;
    @Autowired
    private ActeurRepository acteurRepository;

    @Autowired
    private BpmnRepository bpmnRepository;
    @Autowired
    private MfcRepository mfcRepository;
    @Autowired
    private MctaRepository mctaRepository;

   private  Utilisateur utilisateurCourant; 
   private ConnectionLdap connectionLdap = new  ConnectionLdap();

   private Dico dico = new Dico();
   List<String> bpmnflux = new ArrayList<String>();
   List<String> mfcflux = new ArrayList<String>();
   
   List<String> bpmnacteur = new ArrayList<String>();
   List<String> mfcacteur = new ArrayList<String>();
   
   List<String> bpmnactivite = new ArrayList<String>();
   List<String> mfcactivite = new ArrayList<String>();
   
   private String message = "Bonjour";

   public String getMessage() {
     return message;
   }

    @GetMapping("/index")
    public String indexForm() {
        return "index";
    }

    @GetMapping("/projets")
    public String projets() {
        return "projets";
    }

    @PostMapping("/connexion")
    public String connection(@RequestParam("login") String login, @RequestParam("mdp") String mdp, Model model) {
       
        if (connectionLdap.authUser(login, mdp)) { // Mise en place du ldap

            return  projet();
        } else {
        	  model.addAttribute("message", "Login successful!");
            return "index";
        }
    }
    @PostMapping("/creation")
    public String creation(@RequestParam("login") String login,@RequestParam("mail") String mail, @RequestParam("mdp") String mdp, Model model) {
       
    	
        boolean result = connectionLdap.addUser(login, mail, mdp);
    	System.out.print(result );

        if (result) {
        	   model.addAttribute("creation", "Creation successful!");
        	   
        	   

            Utilisateur user = new Utilisateur();
            user.setPseudonyme(login);
            user.setAdresseMail(mail);
            user.setAdresseMail(mdp);
            utilisateurRepository.save(user);
            

            return "index";
        } else {
            System.out.print(" fail" );
     	   model.addAttribute("creation", "Creation fail !");


            return "index";
        }
    }




    @RequestMapping("/home")
    public String home() {

        return "home";
    }

    @RequestMapping("/projet")
    public String projet() {
        return "projets";
    }

    @PostMapping("/mfc")
    public String handleMFCUpload(@RequestParam("file1") MultipartFile file) throws IllegalStateException, IOException {
        if (file.isEmpty()) {

            System.out.print(file.getOriginalFilename() + "vide!");
        }

        File file2 = new File("C:/Users/ishim/OneDrive/Bureau/e-storymap_equipe_violette/temp/mfc");
        file.transferTo(file2);
        ParserModel mM = new ParserModel();
        mM.getAllElements(file2);
            
        System.out.println("MFC\n");
        System.out.println(mM.getFlux());

        Mfc jpaMfc = new Mfc();
        mfcflux = mM.getFlux();
        mfcacteur =mM.getAuthors();
        mfcactivite =mM.getActivity();
        for(String s : mM.getFlux() ){

            Flux flux = new Flux();
            flux.setMessage(s);
            fluxRepository.save(flux);
            jpaMfc.addFluxMfc(flux);
            

        }


            for(String actr : mM.getAuthors() ){

            Acteur acteur = new Acteur();
            acteur.setNomActeur(actr);
            acteurRepository.save(acteur);
            jpaMfc.addActeurs(acteur);
        }
            mfcRepository.save(jpaMfc);


        System.out.println(fluxRepository.findAll()+"\n");
        System.out.println(activiteRepository.findAll()+"\n");
        System.out.println(acteurRepository.findAll()+"\n");




        return "home";
    }

    @PostMapping("/bpmn")
    public String handleBPMNUpload(@RequestParam("file2") MultipartFile file)
            throws IllegalStateException, IOException {
        if (file.isEmpty()) {

            System.out.print(file.getOriginalFilename() + "vide!");
        }
        File file2 = new File("C:/Users/ishim/OneDrive/Bureau/e-storymap_equipe_violette/temp/bpmn");
        file.transferTo(file2);
        ParserBpmn bpmnM = new ParserBpmn(file2);
        System.out.println("BPMN\n");
        // System.out.println(bpmnM.getFlux() + "\n");
        // System.out.println(bpmnM.getAllTasks());

        Bpmn jpaBpmn = new Bpmn();
        bpmnflux = bpmnM.getFlux();
        bpmnacteur = bpmnM.getAuthors();
        bpmnactivite = bpmnM.getTasks();
        
        for(String s : bpmnM.getFlux() ){

            Flux flux = new Flux();
            flux.setMessage(s);
            fluxRepository.save(flux);
            jpaBpmn.addFluxBpmn(flux);
            

        }

        for(String a : bpmnM.getTasks() ){

            Activite activite = new Activite();
            activite.setNomActivite(a);
            activiteRepository.save(activite);
            jpaBpmn.addActiviteBpmn(activite);
        }

            for(String actr : bpmnM.getAuthors() ){

            Acteur acteur = new Acteur();
            acteur.setNomActeur(actr);
            acteurRepository.save(acteur);
            jpaBpmn.addActeurBpmn(acteur);
        }
            bpmnRepository.deleteAll();
            bpmnRepository.save(jpaBpmn);



        return "home";

    }

    @PostMapping("/mcta")
    public String handleMCTAUpload(@RequestParam("file3") MultipartFile file) throws IllegalStateException, IOException {
        if (file.isEmpty()) {

            System.out.print(file.getOriginalFilename() + "vide!");
        }

        File file2 = new File("C:/Users/Toshiba/Desktop/e-storymap_equipe_violette/mcta");
        file.transferTo(file2);
        ParserModel mM = new ParserModel();
        mM.getAllElements(file2);
        System.out.println("MCTA\n");
        System.out.println(mM.getFlux());

        return "home";
    }



    @RequestMapping("/page3")
    @ModelAttribute("pourcentage")
    public List<Integer> page3() {
    	 ParserModel modelBpmn = new ParserModel();
    	   
          // modelBpmn.setFlux(dico.getTotalFlux());
        //   modelBpmn.setAuthors(dico.getTotalAuthors());
          // modelBpmn.setFlux(dico.getTotalFlux());
           ParserModel modelMfc = new ParserModel();
           
           modelMfc.setActivity(bpmnacteur);
           modelMfc.setAuthors(bpmnactivite);
           modelMfc.setFlux(bpmnflux);
          
           ParserModel modelMcta = new ParserModel();
           
          ModelComparator modelC = new ModelComparator(modelMfc,modelBpmn, modelMcta);
          System.out.print("normalement je commence ");
         	System.out.print( modelC.compareModels().get(0));
         	System.out.print( modelC.compareModels().get(1));


            // System.out.print( );
        return modelC.compareModels()  ;
    }

    @GetMapping(value = "/dico")
    @ModelAttribute("dictionaire")
    public Map<String, Map<String, List<String>>>  dico(){

        // List<Bpmn> bpmnDepot = bpmnRepository.findAll();
    	
    	;
    	dico.compareElements(mfcflux,bpmnflux);
    	
        dico.compareElements(mfcactivite,bpmnactivite);
        
    
    	Map<String,Map<String,  List<String>>> dictionaire = new HashMap<>();

    	Map<String,  List<String>> flux = new HashMap<>();
    	List<String> duplicateflux= new ArrayList<String>();
    	List<String> noDuplicateflux = dico.getTotalFlux();
    	
    	Map<String,  List<String>> acteur = new HashMap<>();
    	List<String> duplicateActeur= new ArrayList<String>();
    	List<String> noDuplicateActeur = bpmnacteur ;
    	
    	Map<String,  List<String>>  activite= new HashMap<>();
    	List<String> duplicateActivite= new ArrayList<String>();
    	List<String> noDuplicateActivite = bpmnactivite;


        dico.numberDuplication(noDuplicateflux, duplicateflux);
        
        dico.numberDuplication(noDuplicateActeur, duplicateActeur);
        
        dico.numberDuplication(noDuplicateActivite, duplicateActivite);



        
        flux.put("test", noDuplicateflux);
        flux.put("dico",duplicateflux );
        
        acteur.put("test", noDuplicateActeur);
        acteur.put("dico",duplicateActeur );
        
        activite.put("test", noDuplicateActivite);
        activite.put("dico",duplicateActivite);
        
        
        
     

        dictionaire.put("flux",flux );
        dictionaire.put("acteur",acteur );
        dictionaire.put("active",activite );
        
   
  
       
        return dictionaire;
    }
    


   // public void getFlux(List<String> flux, )

    //  @PostMapping("/inscription")
    //  public String inscription(@RequestParam("login") String login,@RequestParam("mail")String mail,@RequestParam("mdp") String mdp) {
    //      System.out.print("le login : " +login +"\n");
    //      System.out.print("le mail : "+login+"\n");
    //      System.out.print("le mot de passe : "+mdp);
         
    //      return "index";
    //   }

}
