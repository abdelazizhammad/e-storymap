package com.violet.estorymap.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.violet.estorymap.models.*;
import com.violet.estorymap.models.repository.*;


@RestController
public class modelControlleur {


	@Autowired
	private ActeurRepository acteurRepository;
	@Autowired
	private UtilisateurRepository utilisateurRepository;
    @Autowired
	private FluxRepository fluxRepository;
	@Autowired
	private ActiviteRepository activiteRepository;
	@Autowired
	private ProjetRepository projetRepository;
	@Autowired
	private FichierRepository fichierRepository;
	@Autowired
	private BpmnRepository bpmnRepository;
	@Autowired
	private MfcRepository mfcRepository;
	@Autowired
	private MctaRepository mctaRepository;
	

	@GetMapping("/flux")
	public List<Flux> findAllFlux(){
		
		Flux  f1 = new  Flux();
		f1.setMessage("message 1");
		
		Flux  f2 = new  Flux();
		f2.setMessage("message 2");
		
		this.fluxRepository.save(f1);
		this.fluxRepository.save(f2);
		return this.fluxRepository.findAll();
		
		
	}
	
	
	@GetMapping("/acteur")
	public List<Acteur> findAllActeur(){

		Acteur a1 = new  Acteur();
		a1.setNomActeur("nom 1");
		Acteur a2 = new  Acteur();
		a2.setNomActeur("nom 2");
		
		this.acteurRepository.save(a1);
		this.acteurRepository.save(a2);
		return this.acteurRepository.findAll();
		
		
	}
	
	
	@GetMapping("/activite")
	public List<Activite> findAllActivite(){
		

		Activite a1 = new  Activite();
		a1.setNomActivite("nom 1");

		Activite a2 = new  Activite();
		a2.setNomActivite("nom 2");
		
		this.activiteRepository.save(a1);
		this.activiteRepository.save(a2);
		return this.activiteRepository.findAll();
	}
	
	@GetMapping("/utilisateur")
	public List<Utilisateur> findAllUtilisateur(){
		

		Utilisateur u1 = new  Utilisateur();
		u1.setNomUtilisateur("nom 1");

        Utilisateur u2 = new  Utilisateur();
		u2.setNomUtilisateur("nom 2");
		
		this.utilisateurRepository.save(u1);
		this.utilisateurRepository.save(u2);
		return this.utilisateurRepository.findAll();
	}
	
	
	
	@GetMapping("/projet")
	public List<Projet> findAllProjet(){
		

		Projet p1 = new  Projet();
		p1.setNomProjet("nom 1");
		p1.setLienProjet("lien 1");
		Projet p2 = new  Projet();
		p2.setNomProjet("nom 2");
		p2.setLienProjet("lien 2");

		
	    this.projetRepository.save(p1);
		this.projetRepository.save(p2);
		return this.projetRepository.findAll();
	}
	
	
	@GetMapping("/fichier")
	public List<Fichier> findAllFichier(){
		

		Fichier f1 = new  Fichier();
		f1.setTypeFichier("type 1");
		
		Fichier f2 = new  Fichier();
		f2.setTypeFichier("type 2");
		
	    this.fichierRepository.save(f1);            //ajouterFichier
		this.fichierRepository.save(f2);
		return this.fichierRepository.findAll();         //ListeFichiers
	}
	
	@GetMapping("/bpmn")
	public List<Bpmn> findAllBpmn(){

		Bpmn b1 = new  Bpmn();
		b1.addFluxBpmn(null);
		Bpmn b2 = new  Bpmn();
		b2.addFluxBpmn(null);
		
        this.bpmnRepository.save(b1);
		this.bpmnRepository.save(b2);
		return this.bpmnRepository.findAll();
	}
	
	@GetMapping("/mfc")
	public List<Mfc> findAllMfc(){

		Mfc m1 = new Mfc();
		m1.addActeurs(null);
		Mfc m2 = new Mfc();
		m2.addActeurs(null);
		
        this.mfcRepository.save(m1);
		this.mfcRepository.save(m2);
		return this.mfcRepository.findAll();
	}
	
	@GetMapping("/mcta")
	public List<Mcta> findAllMcta(){

		Mcta m1 = new Mcta();
		m1.addActeurMcta(null);
		Mcta m2 = new Mcta();
		m2.addActeurMcta(null);
		
        this.mctaRepository.save(m1);
		this.mctaRepository.save(m2);
		return this.mctaRepository.findAll();
	}
	
	
	
	

}
