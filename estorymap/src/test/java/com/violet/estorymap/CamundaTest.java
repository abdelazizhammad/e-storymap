package com.violet.estorymap;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.violet.estorymap.metier.parser.ParserBpmn;
import com.violet.estorymap.metier.parser.ParserModel;

import org.camunda.bpm.model.bpmn.*;
import org.camunda.bpm.model.bpmn.instance.IntermediateCatchEvent;
import org.camunda.bpm.model.bpmn.instance.IntermediateThrowEvent;
import org.camunda.bpm.model.bpmn.instance.MessageFlow;
import org.camunda.bpm.model.bpmn.instance.camunda.CamundaConnector;
import org.camunda.bpm.model.bpmn.instance.camunda.CamundaTaskListener;
import org.camunda.bpm.model.xml.ModelInstance;
import org.hamcrest.DiagnosingMatcher;

public class CamundaTest {

    private BpmnModelInstance modelInstance;
    // private Process process;
    static List<String> liste = new ArrayList<String>();
    

    public CamundaTest(File f){
        this.modelInstance = Bpmn.readModelFromFile(f);
        // this.process = modelInstance.getModelElementsByType(Process.class).iterator().next();
        this.liste=liste;
    }
    public void getFlux(){
        Collection<MessageFlow> flux = modelInstance.getModelElementsByType(MessageFlow.class);
        for (MessageFlow m: flux){
            if(m.getName() != null){
          
              // Supprimer les accents
              String chaineSansAccents = java.text.Normalizer.normalize(m.getName(), java.text.Normalizer.Form.NFD)
              .replaceAll("\\p{InCombiningDiacriticalMarks}+", "");

             // Convertir en minuscules
              // Convertir en minuscules et supprimer les espaces
              String chaineEnMinusculesSansEspaces = chaineSansAccents.toLowerCase().replaceAll("\\s+", "").replace(":", "");

              // Extraire la chaîne "F17 : Retour sur l'émission"
              String resultat3 = chaineEnMinusculesSansEspaces.substring(0, chaineEnMinusculesSansEspaces.length());
              
               System.out.println("resultat3 " + resultat3);
            liste.add(resultat3);
            
            }
        }
        Collection<IntermediateCatchEvent> flux2 = modelInstance.getModelElementsByType(IntermediateCatchEvent.class);    
        for(IntermediateCatchEvent m2: flux2){
          
            if(m2.getName() != null){
           
              // Supprimer les accents
              String chaineSansAccents = java.text.Normalizer.normalize(m2.getName(), java.text.Normalizer.Form.NFD)
              .replaceAll("\\p{InCombiningDiacriticalMarks}+", "").replace(":", "");

             // Convertir en minuscules
              // Convertir en minuscules et supprimer les espaces
              String chaineEnMinusculesSansEspaces = chaineSansAccents.toLowerCase().replaceAll("\\s+", "");

              // Extraire la chaîne "F17 : Retour sur l'émission"
              String resultat3 = chaineEnMinusculesSansEspaces.substring(0, chaineEnMinusculesSansEspaces.length());
              
               System.out.println("resultat3 " + resultat3);
               liste.add(resultat3);
            }
            
        
        }

        Collection<IntermediateThrowEvent> flux3 = modelInstance.getModelElementsByType(IntermediateThrowEvent.class);    
        for(IntermediateThrowEvent m3: flux3){
          
            if(m3.getName() != null){
          
            // Supprimer les accents
            String chaineSansAccents = java.text.Normalizer.normalize(m3.getName(), java.text.Normalizer.Form.NFD)
            .replaceAll("\\p{InCombiningDiacriticalMarks}+", "").replace(":", "");

           // Convertir en minuscules
            // Convertir en minuscules et supprimer les espaces
            String chaineEnMinusculesSansEspaces = chaineSansAccents.toLowerCase().replaceAll("\\s+", "");

            // Extraire la chaîne "F17 : Retour sur l'émission"
            String resultat3 = chaineEnMinusculesSansEspaces.substring(0, chaineEnMinusculesSansEspaces.length());
            
             System.out.println("resultat3 " + resultat3);
             
          
        }}
    }


public static void main(String[]args){

    File file = new File ("./diagram611.bpmn");
    File file1 = new File ("./mfc.xml");
		ParserBpmn parser=new ParserBpmn(file);
		System.out.println(parser.getFlux());
		System.out.println(parser.getAuthors());
		System.out.println(parser.getAllTasks());
        ParserModel parser1=new ParserModel();
      // parser1.getAllElements(file1);
       //System.out.println(parser1.getAuthors());
      //System.out.println(parser1.getFlux());
       //System.out.println(parser1.getActivity());
      
    
       
      
       

}
}








    





