package com.violet.estorymap.ldap;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

class ConnectionLdapTest {

    @Test
    void testNewConnection() {
        ConnectionLdap connection = new ConnectionLdap();
        assertNotNull(connection.connection);
    }

    @Test
    void testAuthUser() {
        assertTrue(ConnectionLdap.authUser("testuser", "testpassword"));
        assertFalse(ConnectionLdap.authUser("invaliduser", "invalidpassword"));
    }

    @Test
    void testAddUser() {
        ConnectionLdap connection = new ConnectionLdap();
        assertTrue(connection.addUser("testuser", "testuser@example.com", "testpassword"));
    }

}
