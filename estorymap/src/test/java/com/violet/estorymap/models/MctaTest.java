package com.violet.estorymap.models;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MctaTest {

    private Mcta mcta;

    @BeforeEach
    public void init() {
        mcta = new Mcta();
    }

    @Test
    public void testLesActivitesMcta() {
        Activite activite1 = new Activite();
        activite1.setIdActivite(1L);
        Activite activite2 = new Activite();
        activite2.setIdActivite(2L);
        Set<Activite> lesActivitesMcta = new HashSet<>();
        lesActivitesMcta.add(activite1);
        lesActivitesMcta.add(activite2);
        mcta.setLesActivitesMcta(lesActivitesMcta);
        assertEquals(lesActivitesMcta, mcta.getLesActivitesMcta());
    }

    @Test
    public void testLesFluxMcta() {
        Flux flux1 = new Flux();
        flux1.setIdFlux(1L);
        Flux flux2 = new Flux();
        flux2.setIdFlux(2L);
        Set<Flux> lesFluxMcta = new HashSet<>();
        lesFluxMcta.add(flux1);
        lesFluxMcta.add(flux2);
        mcta.setLesFluxMcta(lesFluxMcta);
        assertEquals(lesFluxMcta, mcta.getLesFluxMcta());
    }

    @Test
    public void testLesActeursMcta() {
        Acteur acteur1 = new Acteur();
        acteur1.setIdActeur(1L);
        Acteur acteur2 = new Acteur();
        acteur2.setIdActeur(2L);
        Set<Acteur> lesActeursMcta = new HashSet<>();
        lesActeursMcta.add(acteur1);
        lesActeursMcta.add(acteur2);
        mcta.setLesActeursMcta(lesActeursMcta);
        assertEquals(lesActeursMcta, mcta.getLesActeursMcta());
    }

    @Test
    public void testIdFichier() {
        Long idFichier = 1L;
        mcta.setIdFichier(idFichier);
        assertEquals(idFichier, mcta.getIdFichier());
    }

    @Test
    public void testTypeFichier() {
        String typeFichier = "MCTA";
        mcta.setTypeFichier(typeFichier);
        assertEquals(typeFichier, mcta.getTypeFichier());
    }

    @Test
    public void testProjet() {
        Projet projet = new Projet();
        projet.setIdProjet(1L);
        mcta.setProjet(projet);
        assertNotNull(mcta.getProjet());
        assertEquals(projet.getIdProjet(), mcta.getProjet().getIdProjet());
    }
}
