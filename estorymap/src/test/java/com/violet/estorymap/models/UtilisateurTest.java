package com.violet.estorymap.models;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UtilisateurTest {

	@Test
	public void testfichier() {
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setNomUtilisateur("nom utilisateur");
		utilisateur.setPrenomUtilisateur("prenom utilisateur");
		utilisateur.setAdresseMail("adresse mail utilisateur");
		utilisateur.setPseudonyme("pseudonyme utilisateur");

		
		// Test getIdutilisateur and setIdutilisateur methods
		Long id = 1L;
		utilisateur.setIdUtilisateurt(id);
		assertEquals(id, utilisateur.getIdUtilisateur());
		

	
		// Test getNomUtilisateur et getPrenomUtilisateur et getAdresseMail et getPseudonyme methods
		assertEquals("nom utilisateur", utilisateur.getNomUtilisateur());
		assertEquals("prenom utilisateur", utilisateur.getPrenomUtilisateur());
		assertEquals("adresse mail utilisateur", utilisateur.getAdresseMail());
		assertEquals("pseudonyme utilisateur", utilisateur.getPseudonyme());
		
	}

}


