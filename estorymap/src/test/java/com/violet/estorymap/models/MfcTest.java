package com.violet.estorymap.models;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MfcTest {
    @InjectMocks
    private Mfc mfc;
    @Mock
    private Flux flux1;
    @Mock
    private Flux flux2;
    @Mock
    private Acteur acteur1;
    @Mock
    private Acteur acteur2;
    @Test
    void gettersAndSettersTest() {
        Set<Flux> fluxSet = new HashSet<>();
        fluxSet.add(flux1);
        fluxSet.add(flux2);
        Set<Acteur> acteurSet = new HashSet<>();
        acteurSet.add(acteur1);
        acteurSet.add(acteur2);
        mfc.addFluxMfc(flux1);
        mfc.addFluxMfc(flux2);
        mfc.addActeurs(acteur1);
        mfc.addActeurs(acteur2);
        assertEquals(fluxSet, mfc.getlesFlux());
        assertEquals(acteurSet, mfc.getlesActeurs());
        assertNotNull(mfc.getIdFichier());
        assertEquals("Mfc", mfc.getTypeFichier());
    }
}
