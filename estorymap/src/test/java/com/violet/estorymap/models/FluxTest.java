package com.violet.estorymap.models;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class FluxTest {

	@Test
	public void testFlux() {
		Flux flux = new Flux();
		flux.setMessage("Test message");
		
		// Test getIdFlux and setIdFlux methods
		Long id = 1L;
		flux.setIdFlux(id);
		assertEquals(id, flux.getIdFlux());
		
		// Test getListeDesMcta and addMcta methods
		Mcta mcta1 = new Mcta();
		Mcta mcta2 = new Mcta();
		Set<Mcta> mctas = new HashSet<Mcta>();
		mctas.add(mcta1);
		mctas.add(mcta2);
		flux.addMcta(mcta1);
		flux.addMcta(mcta2);
		assertEquals(mctas, flux.getListeDesMcta());
		
		// Test getListeDesBpmn and addBpmn methods
		Bpmn bpmn1 = new Bpmn();
		Bpmn bpmn2 = new Bpmn();
		Set<Bpmn> bpmns = new HashSet<Bpmn>();
		bpmns.add(bpmn1);
		bpmns.add(bpmn2);
		flux.addBpmn(bpmn1);
		flux.addBpmn(bpmn2);
		assertEquals(bpmns, flux.getListeDesBpmn());
		
		// Test getListeDesMfc and addMfc methods
		Mfc mfc1 = new Mfc();
		Mfc mfc2 = new Mfc();
		Set<Mfc> mfcs = new HashSet<Mfc>();
		mfcs.add(mfc1);
		mfcs.add(mfc2);
		flux.addMFc(mfc1);
		flux.addMFc(mfc2);
		assertEquals(mfcs, flux.getListeDesMfc());
		
		// Test getMessage method
		assertEquals("Test message", flux.getMessage());
	}

}

