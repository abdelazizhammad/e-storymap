package com.violet.estorymap.models;

import static org.junit.Assert.assertEquals;
import org.junit.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.*;
import org.junit.jupiter.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ActeurTest {

	@org.junit.jupiter.api.Test
	public void testActeur() {
		Acteur acteur = new Acteur();
		acteur.setNomActeur("nomActeur");
		
		// Test getIdActeur and setIdActeur methods
		Long id = 1L;
		acteur.setIdActeur(id);
		assertEquals(id, acteur.getIdActeur());
		
		// Test getListeDesMcta and addMcta methods
		Mcta mcta1 = new Mcta();
		Mcta mcta2 = new Mcta();
		Set<Mcta> mctas = new HashSet<Mcta>();
		mctas.add(mcta1);
		mctas.add(mcta2);
		acteur.addMcta(mcta1);
		acteur.addMcta(mcta2);
		assertEquals(mctas, acteur.getListeDesMcta());
		
		// Test getListeDesBpmn and addBpmn methods
		Bpmn bpmn1 = new Bpmn();
		Bpmn bpmn2 = new Bpmn();
		Set<Bpmn> bpmns = new HashSet<Bpmn>();
		bpmns.add(bpmn1);
		bpmns.add(bpmn2);
		acteur.addBpmn(bpmn1);
		acteur.addBpmn(bpmn2);
		assertEquals(bpmns, acteur.getListeDesBpmn());
		
		// Test getListeDesMfc and addMfc methods
		Mfc mfc1 = new Mfc();
		Mfc mfc2 = new Mfc();
		Set<Mfc> mfcs = new HashSet<Mfc>();
		mfcs.add(mfc1);
		mfcs.add(mfc2);
		acteur.addMFc(mfc1);
		acteur.addMFc(mfc2);
		assertEquals(mfcs, acteur.getListeDesMfc());
		
		// Test getNomActeur method
		assertEquals("nomActeur", acteur.getNomActeur());
	}

}


