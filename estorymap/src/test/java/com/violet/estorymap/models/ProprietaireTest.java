package com.violet.estorymap.models;
import static org.junit.jupiter.api.Assertions.*;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class ProprietaireTest {

    private static EntityManagerFactory entityManagerFactory;

    @BeforeAll
    public static void init() {
        entityManagerFactory = Persistence.createEntityManagerFactory("test");
    }

    @AfterAll
    public static void close() {
        entityManagerFactory.close();
    }

    @Test
    public void testAddAndRemoveInvite() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();

        // Create a new Proprietaire and Invite
        Proprietaire proprietaire = new Proprietaire();
        entityManager.persist(proprietaire);
        Invite invite = new Invite();
        entityManager.persist(invite);

        // Add the Invite to the Proprietaire's list of invites
        proprietaire.addInvite(invite);

        // Verify that the invite is in the Proprietaire's list
        Set<Invite> invites = proprietaire.getListeDesInvites();
        assertTrue(invites.contains(invite));

        // Remove the Invite from the Proprietaire's list of invites
        proprietaire.removeInvite(invite);

        // Verify that the invite is no longer in the Proprietaire's list
        invites = proprietaire.getListeDesInvites();
        assertFalse(invites.contains(invite));

        // Clean up
        entityManager.remove(proprietaire);
        entityManager.remove(invite);
        transaction.commit();
        entityManager.close();
    }

    @Test
    public void testAddAndRemoveProjet() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();

        // Create a new Proprietaire and Projet
        Proprietaire proprietaire = new Proprietaire();
        entityManager.persist(proprietaire);
        Projet projet = new Projet();
        entityManager.persist(projet);

        // Add the Projet to the Proprietaire's list of projets
        proprietaire.addProjet(projet);

        // Verify that the Projet is in the Proprietaire's list
        Set<Projet> projets = proprietaire.getProjets();
        assertTrue(projets.contains(projet));

        // Remove the Projet from the Proprietaire's list of projets
        proprietaire.removeProjet(projet);

        // Verify that the Projet is no longer in the Proprietaire's list
        projets = proprietaire.getProjets();
        assertFalse(projets.contains(projet));

        // Clean up
        entityManager.remove(proprietaire);
        entityManager.remove(projet);
        transaction.commit();
        entityManager.close();
    }
}
