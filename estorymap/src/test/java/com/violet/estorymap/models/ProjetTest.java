package com.violet.estorymap.models;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProjetTest {

	@Test
	public void testfichier() {
		Projet projet = new Projet();
		projet.setNomProjet("nom projet");
		projet.setLienProjet("lien projet");

		
		// Test getIdProjet and setIdProjet methods
		Long id = 1L;
		projet.setIdProjet(id);
		assertEquals(id, projet.getIdProjet());
		

	
		// Test getNomProjet et getLienProjet method
		assertEquals("nom projet", projet.getNomProjet());
		assertEquals("lien projet", projet.getLienProjet());
		
	}

}


