package com.violet.estorymap.models;

import static org.junit.Assert.assertEquals;



import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class FichierTest {

	@Test
	public void testfichier() {
		Fichier fichier = new Fichier();
		fichier.setTypeFichier("type fichier");

		
		// Test getIdFichier and setIdFichier methods
		Long id = 1L;
		fichier.setIdFichier(id);
		assertEquals(id, fichier.getIdFichier());
		
		// Test getProjet et setProjet
		Projet p1 = new Projet();
		fichier.setProjet(p1);
		assertEquals(p1, fichier.getProjet());

	
		// Test getTypeFichier method
		assertEquals("type fichier", fichier.getTypeFichier());
	}

}


