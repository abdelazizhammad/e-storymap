package com.violet.estorymap.models;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ActiviteTest {

	@Test
	public void testActeur() {
		Activite activite = new Activite();
		activite.setNomActivite("nom");
		
		// Test getIdActivite and setIdActivite methods
		Long id = 1L;
		activite.setIdActivite(id);
		assertEquals(id, activite.getIdActivite());
		
		// Test getListeDesMcta and addMcta methods
		Mcta mcta1 = new Mcta();
		Mcta mcta2 = new Mcta();
		Set<Mcta> mctas = new HashSet<Mcta>();
		mctas.add(mcta1);
		mctas.add(mcta2);
		activite.addMcta(mcta1);
		activite.addMcta(mcta2);
		assertEquals(mctas, activite.getListeDesMcta());
		
		// Test getListeDesBpmn and addBpmn methods
		Bpmn bpmn1 = new Bpmn();
		Bpmn bpmn2 = new Bpmn();
		Set<Bpmn> bpmns = new HashSet<Bpmn>();
		bpmns.add(bpmn1);
		bpmns.add(bpmn2);
		activite.addBpmn(bpmn1);
		activite.addBpmn(bpmn2);
		assertEquals(bpmns, activite.getlisteDesBpmn());
		
		
		// Test getNomActivite method
		assertEquals("nom", activite.getNomActivite());
	}

}


