package com.violet.estorymap.models;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class InviteTest {

    private Invite invite;
    private Set<Projet> projets;
    private Proprietaire proprietaire;

    @BeforeEach
    void init() {
        invite = new Invite();
        invite.setNomUtilisateur("Girno");
        invite.setPrenomUtilisateur("Abdelaziz");
        invite.setAdresseMail("abdelaziz.girno@example.com");
        invite.setPseudonyme("Aziz");

        projets = new HashSet<>();
        Projet projet1 = new Projet();
        projet1.setNomProjet("Projet 1");
        Projet projet2 = new Projet();
        projet2.setNomProjet("Projet 2");
        projets.add(projet1);
        projets.add(projet2);
        invite.addListeProjetsInvite(projet1);
        invite.addListeProjetsInvite(projet2);

        proprietaire = new Proprietaire();
        proprietaire.setNomUtilisateur("Aziza");
        proprietaire.setPrenomUtilisateur("Lee");
        proprietaire.setAdresseMail("aziza.lee@example.com");
        proprietaire.setPseudonyme("Azz");
        invite.addProprietaire(proprietaire);
    }

    @Test
    void testGetListeProjetsInvite() {
        assertEquals(projets, invite.getListeProjetsInvite());
    }

    @Test
    void testAddListeProjetsInvite() {
        Projet projet3 = new Projet();
        projet3.setNomProjet("Projet 3");
        invite.addListeProjetsInvite(projet3);
        assertTrue(invite.getListeProjetsInvite().contains(projet3));
    }

    @Test
    void testRemoveListeProjetsInvite() {
        Projet projet1 = projets.iterator().next();
        invite.removeListeProjetsInvite(projet1);
        assertFalse(invite.getListeProjetsInvite().contains(projet1));
    }

    @Test
    void testGetLesProprietaires() {
        assertEquals(1, invite.getLesProprietaires().size());
        assertTrue(invite.getLesProprietaires().contains(proprietaire));
    }

    @Test
    void testAddProprietaire() {
        Proprietaire proprietaire2 = new Proprietaire();
        proprietaire2.setNomUtilisateur("Legend");
        proprietaire2.setPrenomUtilisateur("John");
        proprietaire2.setAdresseMail("john.legend@example.com");
        proprietaire2.setPseudonyme("jlegend");
        invite.addProprietaire(proprietaire2);
        assertEquals(2, invite.getLesProprietaires().size());
        assertTrue(invite.getLesProprietaires().contains(proprietaire));
        assertTrue(invite.getLesProprietaires().contains(proprietaire2));
    }

    @Test
    void testEntity() {
        assertNotNull(invite.getIdUtilisateur());
    }
}
