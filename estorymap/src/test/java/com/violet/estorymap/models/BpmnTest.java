import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@Transactional
public class BpmnTest {

    @Autowired
    private EntityManager entityManager;

    @Test
    public void testCreateBpmn() {
        Bpmn bpmn = new Bpmn();
        bpmn.setTypeFichier("testType");

        Activite activite = new Activite();
        activite.setNom("testActivite");
        entityManager.persist(activite);
        bpmn.addActiviteBpmn(activite);

        Acteur acteur = new Acteur();
        acteur.setNom("testActeur");
        entityManager.persist(acteur);
        bpmn.addActeurBpmn(acteur);

        Flux flux = new Flux();
        flux.setNom("testFlux");
        entityManager.persist(flux);
        bpmn.addFluxBpmn(flux);

        entityManager.persist(bpmn);
        entityManager.flush();

        assertNotNull(bpmn.getIdFichier());

        Bpmn foundBpmn = entityManager.find(Bpmn.class, bpmn.getIdFichier());
        assertEquals(bpmn, foundBpmn);
        assertEquals(1, foundBpmn.getlesActivitesBpmn().size());
        assertEquals(1, foundBpmn.getlesActeursBpmn().size());
        assertEquals(1, foundBpmn.getlesFluxBpmn().size());
    }
}
