package com.violet.estorymap.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(Page3Controller.class)
public class Page3ControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetPage() throws Exception {
        mockMvc.perform(get("/page3.html"))
                .andExpect(status().isOk())
                .andExpect(view().name("page3"));
    }
}
