package com.violet.estorymap.controllers;
import com.violet.estorymap.ldap.ConnectionLdap;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import javax.naming.NamingException;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@SpringBootTest
public class LdapContrrollerTest {
    @Mock
    private ConnectionLdap connectionLdap;
    @InjectMocks
    private LdapContrroller ldapController;
    @Test
    public void testAddUser() throws NamingException {
        Mockito.doNothing().when(connectionLdap).addUser("Ajout1", "Ajout@gmail.com", "barakuda");
        assertDoesNotThrow(() -> ldapController.adduser());
        Mockito.verify(connectionLdap, Mockito.times(1)).addUser("Ajout1", "Ajout@gmail.com", "barakuda");
    }
}
