package com.violet.estorymap.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import com.violet.estorymap.models.Acteur;
import com.violet.estorymap.models.Fichier;
import com.violet.estorymap.models.Flux;
import com.violet.estorymap.models.repository.ActeurRepository;
import com.violet.estorymap.models.repository.BpmnRepository;
import com.violet.estorymap.models.repository.FichierRepository;
import com.violet.estorymap.models.repository.FluxRepository;
import com.violet.estorymap.models.repository.MfcRepository;
import com.violet.estorymap.models.repository.MctaRepository;
import com.violet.estorymap.models.repository.ActiviteRepository;
import com.violet.estorymap.models.repository.ProjetRepository;
import com.violet.estorymap.models.repository.UtilisateurRepository;
import com.violet.estorymap.controllers.ModelControlleur;

@SpringBootTest
public class ModelControlleurTest {

    @Autowired
    private ActeurRepository acteurRepository;
    @Autowired
    private UtilisateurRepository utilisateurRepository;
    @Autowired
    private FluxRepository fluxRepository;
    @Autowired
    private ActiviteRepository activiteRepository;
    @Autowired
    private ProjetRepository projetRepository;
    @Autowired
    private FichierRepository fichierRepository;
    @Autowired
    private BpmnRepository bpmnRepository;
    @Autowired
    private MfcRepository mfcRepository;
    @Autowired
    private MctaRepository mctaRepository;
    @Autowired
    private ModelControlleur modelControlleur;
    @Test
    public void testFindAllFlux() {
        List<Flux> fluxList = modelControlleur.findAllFlux();
        assertEquals(2, fluxList.size());
    }

    @Test
    public void testFindAllActeur() {
        List<Acteur> acteurList = modelControlleur.findAllActeur();
        assertEquals(2, acteurList.size());
    }

    @Test
    public void testFindAllActivite() {
        List<Activite> activiteList = modelControlleur.findAllActivite();
        assertEquals(2, activiteList.size());
    }

    @Test
    public void testFindAllUtilisateur() {
        List<Utilisateur> utilisateurList = modelControlleur.findAllUtilisateur();
        assertEquals(2, utilisateurList.size());
    }

    @Test
    public void testFindAllProjet() {
        List<Projet> projetList = modelControlleur.findAllProjet();
        assertEquals(2, projetList.size());
    }

    @Test
    public void testFindAllFichier() {
        List<Fichier> fichierList = modelControlleur.findAllFichier();
        assertEquals(2, fichierList.size());
    }

    @Test
    public void testFindAllBpmn() {
        assertEquals(2, modelControlleur.findAllBpmn().size());
    }

    @Test
    public void testFindAllMfc() {
        assertEquals(2, modelControlleur.findAllMfc().size());
    }

    @Test
    public void testFindAllMcta() {
        assertEquals(2, modelControlleur.findAllMcta().size());
    }
}
