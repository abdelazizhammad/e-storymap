package com.violet.estorymap.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.ui.Model;

@ExtendWith(MockitoExtension.class)
public class IndexControllerTest {

    @Mock
    Model model;

    @Test
    public void testIndexForm() {
        IndexController controller = new IndexController();
        String result = controller.indexForm();
        assertEquals("index", result);
    }

    @Test
    public void testConnectionValid() {
        IndexController controller = new IndexController();
        String result = controller.connection("admin", "test");
        assertEquals("page3", result);
    }

    @Test
    public void testConnectionInvalid() {
        IndexController controller = new IndexController();
        String result = controller.connection("user", "password");
        assertEquals("index", result);
    }

    @Test
    public void testHome() {
        IndexController controller = new IndexController();
        String result = controller.home();
        assertEquals("home", result);
    }

    @Test
    public void testPage3() {
        IndexController controller = new IndexController();
        String result = controller.page3();
        assertEquals("page3", result);
    }
}
