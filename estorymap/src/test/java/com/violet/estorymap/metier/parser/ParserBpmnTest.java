package com.violet.estorymap.metier.parser;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.File;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("ParserBpmn Test")
public class ParserBpmnTest {

    private ParserBpmn parser;
    /*
    * We assumes that the BPMN file being parsed has :
    * two tasks named "task1" and "task2"
    * two participants named "participant1" and "participant2"
    * and three message flows named "flux1", "flux2", and "flux3".
     * */
    @BeforeEach
    void setUp() {
        File file = new File("path/to/file.bpmn");
        parser = new ParserBpmn(file);
    }

    @Test
    @DisplayName("Test getFlux")
    void testGetFlux() {
        List<String> flux = parser.getFlux();
        assertNotNull(flux);
        assertEquals(3, flux.size());
        assertEquals("flux1", flux.get(0));
        assertEquals("flux2", flux.get(1));
        assertEquals("flux3", flux.get(2));
    }

    @Test
    @DisplayName("Test getAuthors")
    void testGetAuthors() {
        List<String> authors = parser.getAuthors();
        assertNotNull(authors);
        assertEquals(2, authors.size());
        assertEquals("participant1", authors.get(0));
        assertEquals("participant2", authors.get(1));
    }

    @Test
    @DisplayName("Test getTasks")
    void testGetTasks() {
        List<String> tasks = parser.getTasks();
        assertNotNull(tasks);
        assertEquals(2, tasks.size());
        assertEquals("task1", tasks.get(0));
        assertEquals("task2", tasks.get(1));
    }

    @Test
    @DisplayName("Test getAllTasks")
    void testGetAllTasks() {
        List<String> allTasks = parser.getAllTasks();
        assertNotNull(allTasks);
        assertEquals(2, allTasks.size());
        assertEquals("task1", allTasks.get(0));
        assertEquals("task2", allTasks.get(1));
    }

    @Test
    @DisplayName("Test convertString")
    void testConvertString() {
        String converted = parser.convertString("Événement intermédiaire: message");
        assertEquals("evenementintermediairemessage", converted);
    }
}
