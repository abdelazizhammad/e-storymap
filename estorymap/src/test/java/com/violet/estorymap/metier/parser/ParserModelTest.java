package com.violet.estorymap.metier.parser;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ParserModelTest {
    private ParserModel parserModel;
    @BeforeEach
    public void setUp() {
        parserModel = new ParserModel();
    }
    @Test
    public void testGetAllElements() {
        File file = new File("path/to/testfile.xml");
        parserModel.getAllElements(file);
        List<String> allElements = parserModel.getListAllElements();
        assertEquals(3, allElements.size());
        assertTrue(allElements.contains("element1"));
        assertTrue(allElements.contains("element2"));
        assertTrue(allElements.contains("element3"));
    }

    @Test
    public void testConvertString() {
        String input = "Élément1 : avec accents";
        String expectedOutput = "element1avecaccents";
        String actualOutput = parserModel.convertString(input);
        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testTri() {
        parserModel.tri("f1");
        parserModel.tri("p1");
        parserModel.tri("a1");
        List<String> flux = parserModel.getFlux();
        List<String> authors = parserModel.getAuthors();
        List<String> activities = parserModel.getActivity();
        assertEquals(1, flux.size());
        assertTrue(flux.contains("f1"));
        assertEquals(1, authors.size());
        assertTrue(authors.contains("p1"));
        assertEquals(1, activities.size());
        assertTrue(activities.contains("a1"));
    }
}
