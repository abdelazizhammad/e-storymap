

<h1 align="center">E-StoryMap_equipe_violette</h1>
<h2 align="center">


</h2>



## E-STORY-MAP

E-STORY-MAP a pour but d’aider l’analyste à comparer et vérifier la cohérence entre un MFC (Modèle de Flux Conceptuel), un BPMN (Business Process Model and Notation ) et un MCT(Modèle Conceptuel de Traitement).

OBJECTIF : 
L'objectif principal est de l’aider à détecter les erreurs syntaxiques qui peuvent se glisser dans la conception de ses modèles et ainsi pouvoir arriver à la fin de son étude avec des modèles cohérents.

Notre application est avant tout un outil pédagogique. Il permet à l’analyste de prendre conscience des différentes lacunes qui peuvent être dans la conception de ses modèles et de s’améliorer en vérifiant la cohérence à chaque fois.


## Installation 

 - Pour utiliser Camunda Model , il suffit juste d'installer l'extension MavenForJava et d'ajouter les dépendances de camundaModel sur le pom.xml du projet


## Technoloies Utilisées

- [ ] Springboot
- [ ] H2
- [ ] Thymeleaf
- [ ] Apace DS
- [ ] Camunda Model

## Fonctionnalités


### Parsing et Creation d'un dictionnaire
Le parsing du Bpmn a été réalisé grace à une API appelée Camunda Model .
Le parsing du MfC et du MCTA étant des fichiers xml  a été réalisé avec des regex.
Le dictionnaire affiche le total des acteurs , activités et flux présents dans les différents modèles en spécifiant s'ils les ont en commun ou pas.


### Gestion des modèles avec JPA avec  H2 
JPA a été utilisé pour créer des entités Java qui sont ensuite persistées dans une base de données relationnelle.
Pour utiliser JPA avec H2, il suffit de configurer les paramètres de connexion à la base de données H2 dans le fichier de configuration pom.xml, puis utiliser les annotations JPA pour spécifier des entités et les relations entre les entités.





## Aide 

Avez-vous des problèmes avec E-Story-Map? 
Contactez-nous  directement l'équipe violette sur discord https://discord.gg/Gh4HNqpZ36.



### Auteurs 

Ce projet a été réalisé par l'équipe violette dans le cadre du cours AFSI, l'équipe est composé de : 

| Nom | Prénom(s) |
| --- | --- |
| TSHIZA | Clovis |
| ISHIMWE | Gabriella Divine |
| COUNDOUL |Adama |
| AHMED SALEM |Mouhemd el Hassene  |
| AIT YAHIA  | Maya   |
| HAMMAD  | Abdelaziz |

 
















